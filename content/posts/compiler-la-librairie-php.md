---
date: 2022-03-28T22:00:00.000Z
description: ""
draft: false
title: Compiler la librairie PHP
tags:
  - Development
  - PHP
categories:
  - Development
  - PHP
---

# Dans l'épisode précédent

Dans mon post précédent, je me renseignais sur des façons d'exécuter du code PHP en C++ avec pour objectif de l'exécuter comme langage de scripting pour un usage en jeu vidéo.

Après avoir étudié la nouvelle FFI de PHP, je suis venu à la conclusion qu'il me faudra compiler PHP en librairie afin de pouvoir l'utiliser convenablement dans mon code, ce qui va être le sujet de ce post.

Comme j'utilise principalement Linux et que le projet est dans un premier temps axé pour Linux, les instructions seront uniquement disponibles pour Linux.

# Prérequis

S'assurer d'avoir : 

* gcc
* make
* autoconf
* bison
* rec2c
* libtool

## Compilation

Comme on veut garder la librairie PHP dans le projet, on va préciser un fichier dans lequel le projet va se compiler (dans mon cas je vais directement le compiler dans le bon dossier de mon projet).

On va aussi désactiver toutes les fonctions qui nous seront inutiles et ne faire appel à aucune extensions (dans un premier temps) pour faciliter le processus.

```sh

./buildconf

./configure --prefix=/home/USERNAME(/Projects/php_scripting/extern/php --enable-embed=static --disable-cli --enable-opcache --disable-cgi --disable-all

```

Et finalement on compile et on copie les fichiers avec :

```sh

make -j16
make install

```

# Source

[https://www.phpinternalsbook.com/php7/build_system/building_php.html](https://www.phpinternalsbook.com/php7/build_system/building_php.html)

[https://thephp.website/en/issue/how-to-compile-php/](https://thephp.website/en/issue/how-to-compile-php/)
