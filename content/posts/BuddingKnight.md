---
title: "Budding Knight"
description : ""
date: 2021-09-16
tags : ["Development", "C++"]
categories : ["Development", "Unreal"]
draft : false
---

# Le projet

Projet scolaire au sein d'ISART digital Paris sur 3 mois sur le theme du *brawler*

# Description

todo

# Équipe

todo

# Téléchargement
Veuillez trouver le lien de téléchargement a ce lien (itch.io): [BuddingKnight](https://leogrard.itch.io/buddingknight)


# Captures d'écran

La map de notre niveau, pensée pour mettre a profit la dextérité du joueur  
![budding knight map](https://img.itch.zone/aW1hZ2UvODI5ODg5LzQ2NTEyODIucG5n/original/Sp734V.png)
L'écran principal ou notre joueur se familiarisera avec le concept et les controles
![budding knight lobby](https://img.itch.zone/aW1hZ2UvODI5ODg5LzQ2NTE1OTcucG5n/original/1FkRGV.png)
En jeu le joueur devra se battre contre des vagues d'ennemis a l'aide de son épée et de ses plantes
![budding knight in game](https://img.itch.zone/aW1hZ2UvODI5ODg5LzQ2NTE1OTYucG5n/original/L6JTm1.png)