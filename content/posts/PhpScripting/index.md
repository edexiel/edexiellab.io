---
title: Scripting en PHP
date: 2021-11-03T13:20:41.000+00:00
description: ''
tags:
- Development
- PHP
categories:
- Development
- PHP

---
## Contexte

Cette recherche et développement s'inscrit dans le cadre des projets PEA, Projets d'entrée en alternance, pour ma formation à ISART Digital. 

Le PEA prend la forme d'un projet d'une 20ène d'heure minimum répartie tout au long de l'année sous la forme de demi-journées d'autonomie

## Projet

Étudier la viabilité du PHP comme langage de scripting pour le développement de jeu vidéo et documenter mon progrès sur un blog (coucou).

Le scripting dans un moteur de jeu est pour moi une énorme boite noire, développer mon propre système de scripting permettrais de me familiariser avec le concept et les techniques utilisées.  
  
Afin d'ajouter un peu de _piment_, je n'ai pas voulu prendre un langage qui est couramment employé pour ce genre de taches comme le python,C# ou LUA.

## Pourquoi le PHP ?

{{< figure src="images/becauseicanmatrix.jpg">}}



Ayant travaillé dans le développement web coté serveur, mon choix s'est porté sur PHP qui, en plus d'avoir une syntaxe familière de la famille du C et une boite d'outils complets, a vu ces dernières années des efforts de développement et d'optimisation conséquents.

## La suite

* Même si le choix de PHP est acté, il serait intéressant d'évaluer ses performances par rapport aux autres solutions communes pour du scripting.
* Étudier des articles et des projets afin de me familiariser avec le concept
* Étudier PHP pour pouvoir l'intégrer dans le projet
* Commencer le développement en C++ 
* Utiliser la librairie dans le développement d'une démo en utilisant Raylib 

## Sources

[https://itnext.io/c-scripting-alternatives-easy-to-bind-scripting-binding-chaiscript-and-wren-into-a-small-game-174c86b0ecd7](https://itnext.io/c-scripting-alternatives-easy-to-bind-scripting-binding-chaiscript-and-wren-into-a-small-game-174c86b0ecd7 "https://itnext.io/c-scripting..")

[https://thephp.website/en/issue/php-ffi/](https://thephp.website/en/issue/php-ffi/)

[https://github.com/jonigata/squall](https://github.com/jonigata/squall "https://github.com/jonigata/squall")

[https://www.jacksondunstan.com/articles/3938](https://www.jacksondunstan.com/articles/3938 "https://www.jacksondunstan.com/articles/3938")

[https://gameprogrammingpatterns.com/bytecode.html](https://gameprogrammingpatterns.com/bytecode.html "https://gameprogrammingpatterns.com/bytecode.html")