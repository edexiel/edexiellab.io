---
title: "LightEscape"
date: 2021-09-16
tags : ["Development", "C#", "Unity"]
categories : ["Development", "Unity"]
draft: false
---

# Projet

Réalisé a l'occasion de la Global Game Jam 2017 dont le theme était *wave*, Light Escape est un platformer puzzle qui joue sur les propriétés physique de la lumiere pour mettre a mal notre dextérité et notre logique.

![Light escape blue wave](https://ggj.s3.amazonaws.com/styles/game_content__wide/games/screenshots/ingame_4.jpg)


{{< youtube 4kUmmoOWupQ >}}