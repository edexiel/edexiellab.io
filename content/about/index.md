+++
aliases = ["about-us", "cv", "about-me", "curriculum-vitae"]
author = "Guillaume Nisi"
date = 2021-06-11T00:00:00Z
description = "A propos de moi"
title = "Développeur jeux vidéos et web fullstack"

+++
[PDF CV](cv-guillaume-nisi.pdf)

## Formations

**2019/Aujourd’hui**  ISART Digital, Mastère en Game Programming  
**2018/2019**  CNAM, Concepteur en architecture informatique  
**2017/2018**  Licence Professionnelle CDED en alternance  
**2014/2017** DUT informatique  
**2014** Baccalauréat STi2D SIN

## Expérience professionelle

**2017/2019**  Développeur web en alternance Websurg, [IRCAD](https://www.ircad.fr/), Strasbourg  
**2017**  Développeur web, stage de fin d’étude 1G6, Strasbourg  
**Juillet - Août 2014|5|6**  Employé commercial Cora, Saint-Avold

## Réalisations

**2021**  Projet de fin d’année
: Moteur de jeu 3D et mini jeu de démo

**sept./nov. 2020**  Projet scolaire
: Brawler 3D “[Budding Knight](/posts/buddingknight/)”

**nov./dec 2019** Projet scolaire“Rasterizer”
: Création d’un pipeline de rasterisation sur CPU

**Juin 2018** Projet d’entreprise
: Lancement de la v6 de [Websurg.com](https://websurg.com/fr/)

**2017** Projet scolaire volontaire
: Prototype de serious-game avec un doctorant en médecine sur la sédation profonde et continue

**2017** Projet de GlobalGameJam  
: Réalisation en 48h d’un jeu sur le theme “Wave” [Light Escape](/posts/lightescape/)

## Compétences

### Langues

Anglais C1 Allemand base

### Base de données

Oracle DB MariaDb MySql

### Langages

#### Logiciel

C++ C C# Java

#### Web

HTML CSS Javascript PHP

### Moteurs de jeux

Unity UnrealEngine Raylib

### Outils

gdb valgrind CLion  Linux Unix