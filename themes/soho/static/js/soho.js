var callback = function(){

  var twitter_b = document.getElementsByClassName("twitter-share-button")[0];
  twitter_b.addEventListener("click", function (e) {
    e.preventDefault();
    var url = encodeURIComponent(window.location.href);
    var text = encodeURIComponent(twitter_b.getAttribute("data-text"));

    window.open(`https://twitter.com/intent/tweet?text=${text}&url=${url}`, "_blank").focus();
  });
  
  var linkedin_b = document.getElementsByClassName("linkedin-share-button")[0];
  linkedin_b.addEventListener("click", function (e) {
    e.preventDefault();
    var url = encodeURIComponent(window.location.href);

    window.open(`https://www.linkedin.com/sharing/share-offsite/?url=${url}`, "_blank").focus();
  });

  var facebook_b = document.getElementsByClassName("facebook-share-button")[0];
  facebook_b.addEventListener("click", function (e) {
    e.preventDefault();
    var url = encodeURIComponent(window.location.href);

    window.open(`https://www.facebook.com/sharer/sharer.php?u=${url}`, "_blank").focus();
  });

  var telegram = document.getElementsByClassName("telegram-share-button")[0];
  telegram.addEventListener("click", function (e) {
    e.preventDefault();
    var url = encodeURIComponent(window.location.href);
    var text = encodeURIComponent(telegram.getAttribute("data-text"));

    window.open(`https://t.me/share/url?url=${url}&text=${text}`, "_blank").focus();
  });

  var pinterest = document.getElementsByClassName("pinterest-share-button")[0];
  pinterest.addEventListener("click", function (e) {
    e.preventDefault();
    var url = encodeURIComponent(window.location.href);
    var text = encodeURIComponent(pinterest.getAttribute("data-text"));

    window.open(`https://pinterest.com/pin/create/button/?url=${url}&media=&description=${text}`, "_blank").focus();
  });

};

if (
  document.readyState === "complete" ||
  (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
callback();
} else {
document.addEventListener("DOMContentLoaded", callback);
}
